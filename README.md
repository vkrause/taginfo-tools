# OSM Taginfo Tools

Scripts for working with data from [OSM's](https://openstreetmap.org) [Taginfo](https://taginfo.openstreetmap.org) tool.

***Use with extreme care, this can create an unreasonable amount of API calls***

## Set-like tag value statistics

```
./taginfo-key-set-values.py --key <tag key> [--ignore-case] [--trim] [--try-hard]
```

Produces occurence counts for elements in set-like tag values.
By default this follows the [strict semicolon separator rules](https://wiki.openstreetmap.org/wiki/Semi-colon_value_separator),
the additional options enable for normalization and more tolerant value splitting.
