#!/usr/bin/env python3
#
# SPDX-FileCopyrightText: 2023 Volker Krause <vkrause@kde.org>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Retrieve occurence counts for values in set keys from taginfo.
#

import argparse
import json
import re
import requests
import sys
import time

parser = argparse.ArgumentParser(description='Retrieve occurence counts for values in set keys from taginfo')
parser.add_argument('--key', type=str, required=True, help='The tag to query')
parser.add_argument('--ignore-case', help='Treat values case-insensitve', action='store_true')
parser.add_argument('--trim', help='Trim whitespaces from values', action='store_true')
parser.add_argument('--try-hard', help='Go way outside of the spec for splitting lists', action='store_true')
arguments = parser.parse_args()

page_size=500
page_count=1
base_url = f"https://taginfo.openstreetmap.org/api/4/key/values?key={arguments.key}&sortname=count&sortorder=desc&rp={page_size}&page="
request_throttle = 2 # reuqests per second

stats = {}

while True:
    req = requests.get(base_url + str(page_count))
    # print (req.text)
    print('.', end='', file=sys.stderr, flush=True)
    data = json.loads(req.text)
    for entry in data['data']:
        values = []
        if arguments.try_hard:
            values = re.split(r"(?<!;)[,;][ _]*", entry['value'])
        else:
            # see https://wiki.openstreetmap.org/wiki/Semi-colon_value_separator
            values = re.split(r"(?<!;); *", entry['value'])

        for value in values:
            if arguments.ignore_case:
                value = value.lower()
            if arguments.trim:
                value = value.strip()
                if arguments.try_hard:
                    value = value.strip('_')
            if not value in stats:
                stats[value] = 0
            stats[value] += entry['count']
    if len(data['data']) != page_size:
        break
    page_count += 1;
    time.sleep(1/request_throttle)

print('\n', end='', file=sys.stderr, flush=True)
for entry in sorted(stats.items(), key=lambda item: item[1], reverse=True):
    print(f"{entry[0]}: {entry[1]}")
